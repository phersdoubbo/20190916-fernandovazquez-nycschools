package com.chase.v720133.a20190916_fernandovazquez_nycschools.databinding;

import android.databinding.Bindable;
import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.databinding.ViewDataBinding;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.chase.School;

public abstract class ActivitySchoolDetailBinding extends ViewDataBinding {
  @NonNull
  public final TextView editText;

  @NonNull
  public final TextView editText2;

  @NonNull
  public final TextView editText3;

  @NonNull
  public final TextView editText4;

  @NonNull
  public final TextView etSatScore;

  @NonNull
  public final TextView tvMath;

  @NonNull
  public final TextView tvReading;

  @NonNull
  public final TextView tvWriting;

  @Bindable
  protected School mSchool;

  protected ActivitySchoolDetailBinding(DataBindingComponent _bindingComponent, View _root,
      int _localFieldCount, TextView editText, TextView editText2, TextView editText3,
      TextView editText4, TextView etSatScore, TextView tvMath, TextView tvReading,
      TextView tvWriting) {
    super(_bindingComponent, _root, _localFieldCount);
    this.editText = editText;
    this.editText2 = editText2;
    this.editText3 = editText3;
    this.editText4 = editText4;
    this.etSatScore = etSatScore;
    this.tvMath = tvMath;
    this.tvReading = tvReading;
    this.tvWriting = tvWriting;
  }

  public abstract void setSchool(@Nullable School School);

  @Nullable
  public School getSchool() {
    return mSchool;
  }

  @NonNull
  public static ActivitySchoolDetailBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot) {
    return inflate(inflater, root, attachToRoot, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static ActivitySchoolDetailBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable ViewGroup root, boolean attachToRoot, @Nullable DataBindingComponent component) {
    return DataBindingUtil.<ActivitySchoolDetailBinding>inflate(inflater, com.chase.v720133.a20190916_fernandovazquez_nycschools.R.layout.activity_school_detail, root, attachToRoot, component);
  }

  @NonNull
  public static ActivitySchoolDetailBinding inflate(@NonNull LayoutInflater inflater) {
    return inflate(inflater, DataBindingUtil.getDefaultComponent());
  }

  @NonNull
  public static ActivitySchoolDetailBinding inflate(@NonNull LayoutInflater inflater,
      @Nullable DataBindingComponent component) {
    return DataBindingUtil.<ActivitySchoolDetailBinding>inflate(inflater, com.chase.v720133.a20190916_fernandovazquez_nycschools.R.layout.activity_school_detail, null, false, component);
  }

  public static ActivitySchoolDetailBinding bind(@NonNull View view) {
    return bind(view, DataBindingUtil.getDefaultComponent());
  }

  public static ActivitySchoolDetailBinding bind(@NonNull View view,
      @Nullable DataBindingComponent component) {
    return (ActivitySchoolDetailBinding)bind(component, view, com.chase.v720133.a20190916_fernandovazquez_nycschools.R.layout.activity_school_detail);
  }
}
