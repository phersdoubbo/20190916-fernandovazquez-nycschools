package com.chase.v720133.a20190916_fernandovazquez_nycschools.databinding;
import com.chase.v720133.a20190916_fernandovazquez_nycschools.R;
import com.chase.v720133.a20190916_fernandovazquez_nycschools.BR;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
@SuppressWarnings("unchecked")
public class ActivitySchoolDetailBindingImpl extends ActivitySchoolDetailBinding  {

    @Nullable
    private static final android.databinding.ViewDataBinding.IncludedLayouts sIncludes;
    @Nullable
    private static final android.util.SparseIntArray sViewsWithIds;
    static {
        sIncludes = null;
        sViewsWithIds = new android.util.SparseIntArray();
        sViewsWithIds.put(R.id.etSatScore, 5);
        sViewsWithIds.put(R.id.editText2, 6);
        sViewsWithIds.put(R.id.editText3, 7);
        sViewsWithIds.put(R.id.editText4, 8);
    }
    // views
    @NonNull
    private final android.support.constraint.ConstraintLayout mboundView0;
    // variables
    // values
    // listeners
    // Inverse Binding Event Handlers

    public ActivitySchoolDetailBindingImpl(@Nullable android.databinding.DataBindingComponent bindingComponent, @NonNull View root) {
        this(bindingComponent, root, mapBindings(bindingComponent, root, 9, sIncludes, sViewsWithIds));
    }
    private ActivitySchoolDetailBindingImpl(android.databinding.DataBindingComponent bindingComponent, View root, Object[] bindings) {
        super(bindingComponent, root, 0
            , (android.widget.TextView) bindings[1]
            , (android.widget.TextView) bindings[6]
            , (android.widget.TextView) bindings[7]
            , (android.widget.TextView) bindings[8]
            , (android.widget.TextView) bindings[5]
            , (android.widget.TextView) bindings[2]
            , (android.widget.TextView) bindings[3]
            , (android.widget.TextView) bindings[4]
            );
        this.editText.setTag(null);
        this.mboundView0 = (android.support.constraint.ConstraintLayout) bindings[0];
        this.mboundView0.setTag(null);
        this.tvMath.setTag(null);
        this.tvReading.setTag(null);
        this.tvWriting.setTag(null);
        setRootTag(root);
        // listeners
        invalidateAll();
    }

    @Override
    public void invalidateAll() {
        synchronized(this) {
                mDirtyFlags = 0x2L;
        }
        requestRebind();
    }

    @Override
    public boolean hasPendingBindings() {
        synchronized(this) {
            if (mDirtyFlags != 0) {
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean setVariable(int variableId, @Nullable Object variable)  {
        boolean variableSet = true;
        if (BR.School == variableId) {
            setSchool((com.chase.School) variable);
        }
        else {
            variableSet = false;
        }
            return variableSet;
    }

    public void setSchool(@Nullable com.chase.School School) {
        this.mSchool = School;
        synchronized(this) {
            mDirtyFlags |= 0x1L;
        }
        notifyPropertyChanged(BR.School);
        super.requestRebind();
    }

    @Override
    protected boolean onFieldChange(int localFieldId, Object object, int fieldId) {
        switch (localFieldId) {
        }
        return false;
    }

    @Override
    protected void executeBindings() {
        long dirtyFlags = 0;
        synchronized(this) {
            dirtyFlags = mDirtyFlags;
            mDirtyFlags = 0;
        }
        com.chase.School school = mSchool;
        java.lang.String schoolDbn = null;
        java.lang.String schoolScoreWriting = null;
        java.lang.String schoolScoreMath = null;
        java.lang.String schoolScoreReading = null;

        if ((dirtyFlags & 0x3L) != 0) {



                if (school != null) {
                    // read School.dbn
                    schoolDbn = school.dbn;
                    // read School.scoreWriting
                    schoolScoreWriting = school.scoreWriting;
                    // read School.scoreMath
                    schoolScoreMath = school.scoreMath;
                    // read School.scoreReading
                    schoolScoreReading = school.scoreReading;
                }
        }
        // batch finished
        if ((dirtyFlags & 0x3L) != 0) {
            // api target 1

            android.databinding.adapters.TextViewBindingAdapter.setText(this.editText, schoolDbn);
            android.databinding.adapters.TextViewBindingAdapter.setText(this.tvMath, schoolScoreMath);
            android.databinding.adapters.TextViewBindingAdapter.setText(this.tvReading, schoolScoreReading);
            android.databinding.adapters.TextViewBindingAdapter.setText(this.tvWriting, schoolScoreWriting);
        }
    }
    // Listener Stub Implementations
    // callback impls
    // dirty flag
    private  long mDirtyFlags = 0xffffffffffffffffL;
    /* flag mapping
        flag 0 (0x1L): School
        flag 1 (0x2L): null
    flag mapping end*/
    //end
}