/**
 * Automatically generated file. DO NOT MODIFY
 */
package com.chase.v720133.a20190916_fernandovazquez_nycschools;

public final class BuildConfig {
  public static final boolean DEBUG = Boolean.parseBoolean("true");
  public static final String APPLICATION_ID = "com.chase.v720133.a20190916_fernandovazquez_nycschools";
  public static final String BUILD_TYPE = "debug";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 1;
  public static final String VERSION_NAME = "1.0";
}
