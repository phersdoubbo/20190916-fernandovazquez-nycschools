package com.chase;

import android.os.Parcel;
import android.os.Parcelable;

public class SchoolDetail {
    public String dbn;
    public String schoolName;
    public String scoreMath;
    public String scoreReading;
    public String scoreWriting;
    public String color;

    public SchoolDetail(String dbn, String schoolName, String scoreMath, String scoreReading, String scoreWriting)

    {
        this.dbn = dbn;
        this.schoolName = schoolName;
        this.scoreMath = scoreMath;
        this.scoreReading = scoreReading;
        this.scoreWriting = scoreWriting;

    }


}
