package com.chase;

import android.app.Activity;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.chase.School;
import com.chase.v720133.a20190916_fernandovazquez_nycschools.R;
import com.chase.v720133.a20190916_fernandovazquez_nycschools.databinding.ActivitySchoolDetailBinding;

public class SchoolDetailActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school_detail);

        School school = getIntent().getParcelableExtra("School");
        ActivitySchoolDetailBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_school_detail);
        binding.setSchool(school);

    }
}
