package com.chase;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.NonNull;

public class School implements Comparable<School> , Parcelable {
    public String dbn;
    public String schoolName;
    public String scoreMath;
    public String scoreReading;
    public String scoreWriting;
    public String boro;
    public String color;

    public School(String dbn, String schoolName, String boro, SchoolDetail schoolDetail)

    {

        this.dbn = dbn;
        this.schoolName = schoolName;
        this.boro = boro;
        if (schoolDetail != null){
            this.scoreMath = schoolDetail.scoreMath;
            this.scoreReading = schoolDetail.scoreReading;
            this.scoreWriting = schoolDetail.scoreWriting;
            //all these color should be in constants but for the demo i just kept them in here
            // move all these into constants
            this.color = "#456438";
        } else {
            this.scoreMath = "N/A";
            this.scoreReading = "N/A";
            this.scoreWriting = "N/A";
            this.color = "#ff0000";
        }
    }

    protected School(Parcel in) {
        dbn = in.readString();
        schoolName = in.readString();
        scoreMath = in.readString();
        scoreReading = in.readString();
        scoreWriting = in.readString();
        boro = in.readString();
        if (scoreMath.equals("N/A")){
            color = "#ff0000";
        } else {
            color = "#456438";
        }
    }

    public static final Creator<School> CREATOR = new Creator<School>() {
        @Override
        public School createFromParcel(Parcel in) {
            return new School(in);
        }

        @Override
        public School[] newArray(int size) {
            return new School[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(dbn);
        dest.writeString(schoolName);
        dest.writeString(scoreMath);
        dest.writeString(scoreReading);
        dest.writeString(scoreWriting);
        dest.writeString(boro);
    }

    @Override
    public int compareTo(@NonNull School school) {
        return schoolName.compareTo(school.schoolName);
    }
}
