package com.chase.util;

import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.chase.School;
import com.chase.SchoolDetail;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class ApiUtil {

    //Fernando removing the constructor just to use static methods in this class
    private ApiUtil(){}

    public static final String BASE_URL = "https://data.cityofnewyork.us/resource/";
    public static final String SCHOOL_URL = "s3k6-pzi2.json";
    public static final String STATS_URL = "f9bf-2cp4.json";

    public static URL buildUrl(String  resource){
        URL url = null;
        Uri uri = Uri.parse(BASE_URL).buildUpon().appendPath(resource).build();

        try {
            url= new URL(uri.toString());
        }catch (Exception e){
            e.printStackTrace();
        }
        return url;
    }


    public static String getJson(URL url) throws IOException {

        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        InputStream stream = connection.getInputStream();
        //allows you to buffer and utf8 the data
        Scanner scanner = new Scanner(stream);
        //read everything , using pattern for the regular expression
        scanner.useDelimiter("\\A");
        try {


            boolean hasData = scanner.hasNext();
            if (hasData) {
                return scanner.next();
            } else {
                return null;
            }
        } catch(Exception e){
            Log.d("Error", e.toString());
            return null;
        } finally {
            connection.disconnect();
        }
    }

    public static ArrayList<School> getSchoolListFromJson(String json, Map<String, SchoolDetail> stats) {
        //Do not use string, use constants as best practices
        final String DBN = "dbn";
        final String SCHOOL_NAME = "school_name";
        final String BORO = "boro";
        ArrayList<School> books= new ArrayList<School>();
        try {
            JSONObject jsonObject = new JSONObject(json);
            String list = jsonObject.get("schoolList").toString();
            JSONArray jsonSchool = new JSONArray(list);

            //JSONArray jsonSchool = new JSONArray(json);
            int numberSchool = jsonSchool.length();
            for (int i =0; i< numberSchool;i++){
                JSONObject school = jsonSchool.getJSONObject(i);
                String dbn = school.getString(DBN);
                String schoolName = school.getString(SCHOOL_NAME);
                String boro = school.getString(BORO);
                SchoolDetail sd = stats.get(dbn);

                books.add(new School(dbn, schoolName, boro, sd));

            }
        }catch (Exception e){
            Log.d("Error", e.getMessage());
        }
        return books;

    }


    public static Map<String, SchoolDetail> getSchoolStatsFromJson(String json) {
        //Do not use string, use constants as best practices
        final String DBN = "dbn";
        final String SCHOOL_NAME = "school_name";
        final String SCORE_MATH = "sat_math_avg_score";
        final String SCORE_WRITING = "sat_writing_avg_score";
        final String SCORE_READING = "sat_critical_reading_avg_score";
        Map statsMap = new HashMap<String, SchoolDetail>();

        ArrayList<School> books= new ArrayList<School>();
        try {
            JSONObject jsonObject = new JSONObject(json);
            String stats = jsonObject.get("schoolStats").toString();
            JSONArray jsonStats = new JSONArray(stats);

            int numberSchool = jsonStats.length();
            for (int i =0; i< numberSchool;i++){
                JSONObject school = jsonStats.getJSONObject(i);
                String dbn = school.getString(DBN);
                String schoolName = school.getString(SCHOOL_NAME);
                String scoreWriting = school.getString(SCORE_WRITING);
                String scoreMath = school.getString(SCORE_MATH);
                String scoreReading = school.getString(SCORE_READING);
                statsMap.put(dbn, new SchoolDetail(dbn, schoolName, scoreMath, scoreReading, scoreWriting));


            }
        }catch (Exception e){
            Log.d("Error", e.getMessage());
        }
        return statsMap;

    }
}
