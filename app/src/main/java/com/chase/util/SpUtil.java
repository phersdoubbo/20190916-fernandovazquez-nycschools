package com.chase.util;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;
import java.util.Map;

public class SpUtil {

    private SpUtil(){

    }


    public static final String PREF_NAME = "SchoolPreferences";


    public static SharedPreferences getPrefs(Context context){
        return context.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE);
    }

    public static String getPreferenceString (Context context, String key) {
        return getPrefs(context).getString(key, "");
    }

    public static int getPreferenceInt (Context context, String key) {
        return getPrefs(context).getInt(key, 0);
    }


    public static void setPreferenceString  (Context context, String key, String value) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static void setPreferenceInt  (Context context, String key, int value) {
        SharedPreferences.Editor editor = getPrefs(context).edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static String getBoro(String boro) {
        //move this into constants
        Map<String, String> boroName = new HashMap<String,String>();
        boroName.put("X","Bronx");
        boroName.put("K","Brooklyn");
        boroName.put("M","Manhattan");
        boroName.put("Q","Queens");
        if (boro.isEmpty()){
            return "";
        }
        else if (boroName.containsKey(boro)) {
            return boroName.get(boro);
        }
        return boro;
    }

}
