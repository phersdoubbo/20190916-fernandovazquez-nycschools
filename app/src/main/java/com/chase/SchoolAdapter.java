package com.chase;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.chase.util.SpUtil;
import com.chase.v720133.a20190916_fernandovazquez_nycschools.R;

import java.util.ArrayList;

public class SchoolAdapter  extends RecyclerView.Adapter<SchoolAdapter.SchoolViewHolder> {


    ArrayList<School> schools;


    public SchoolAdapter(ArrayList<School> schools){
        this.schools = schools;
    }

    @Override
    public SchoolViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        View itemView = LayoutInflater.from(context).inflate(R.layout.school_list,parent, false);
        return new SchoolViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(SchoolViewHolder holder, int position) {
        School school = schools.get(position);
        holder.bind(school);
    }

    @Override
    public int getItemCount() {
        return schools.size();
    }



    public class SchoolViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView schoolDBN;
        TextView schoolName;

        public SchoolViewHolder(View itemView) {
            super(itemView);
            schoolDBN = (TextView) itemView.findViewById(R.id.schoolDBN);
            schoolName = (TextView) itemView.findViewById(R.id.schoolName);
            itemView.setOnClickListener(this);
        }

        public void bind(School school){

            schoolDBN.setText(school.dbn + " [ " + SpUtil.getBoro(school.boro) + " ]");
            schoolName.setText(school.schoolName);
            schoolDBN.setTextColor(Color.parseColor(school.color));
            schoolName.setTextColor(Color.parseColor(school.color));
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            School selectedSchool = schools.get(position);
            Intent intent = new Intent(v.getContext(), SchoolDetailActivity.class);
            intent.putExtra("School", selectedSchool);
            v.getContext().startActivity(intent);
        }
    }
}
