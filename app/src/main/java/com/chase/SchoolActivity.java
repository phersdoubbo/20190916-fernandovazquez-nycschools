package com.chase;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.SearchView;
import android.widget.TextView;

import com.chase.util.ApiUtil;
import com.chase.util.SpUtil;
import com.chase.v720133.a20190916_fernandovazquez_nycschools.R;

import org.json.JSONObject;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SchoolActivity extends Activity implements SearchView.OnQueryTextListener{

    private ProgressBar mLoadingProgress;
    private RecyclerView rvSchools;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_school);

        mLoadingProgress = (ProgressBar) findViewById(R.id.school_loading);
        rvSchools = (RecyclerView) findViewById(R.id.rv_schools);
        LinearLayoutManager schoolsLayoutManager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL, false);
        rvSchools.setLayoutManager(schoolsLayoutManager);
        try {
            URL schoolList = ApiUtil.buildUrl(ApiUtil.SCHOOL_URL);
            URL schoolStats = ApiUtil.buildUrl(ApiUtil.STATS_URL);
            new SchoolTask().execute(schoolList, schoolStats);

        } catch (Exception e){
            Log.d("Error", e.getMessage()); }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        getMenuInflater().inflate(R.menu.school_list_menu, menu);
        final MenuItem searchItem=menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);
        searchView.setOnQueryTextListener(this);
        return true;
    }


    private static ArrayList<School> getFilterOutput(List<School> schools,String query) {
        ArrayList<School> result = new ArrayList<>();
        query = "" + query.toLowerCase();
        for (School school : schools) {
            //We are just having basic filtering based on keyword contain and * to reset to all values as the empty does not trigger a search
            //there is no regex in this search at this point
            String boro = SpUtil.getBoro(school.boro).toLowerCase();
            if (boro.contains(query) || school.dbn.toLowerCase().contains(query) || school.schoolName.toLowerCase()
                    .contains(query) || query.isEmpty()  || query.contains("*")) {
                result.add(school);
            }
        }
        return result;
    }


    @Override
    public boolean onQueryTextSubmit(String query) {

        try {

            TextView schoolError = (TextView) findViewById(R.id.schoolError);

            mLoadingProgress.setVisibility(View.VISIBLE);

            //we are not calling the service again just reading from SharePreference and setting the values in the recyclerView
            Context context = getApplicationContext();
            String   schoolList = SpUtil.getPreferenceString(context, "schoolList");
            String   schoolStats = SpUtil.getPreferenceString(context, "schoolStats");
            JSONObject resultJSON = new JSONObject();
            try{
                resultJSON.put("schoolList", schoolList);
                resultJSON.put("schoolStats", schoolStats);
            } catch (Exception e){

            }

            ArrayList<School> schools = ApiUtil.getSchoolListFromJson(resultJSON.toString(),  ApiUtil.getSchoolStatsFromJson(resultJSON.toString()));
            ArrayList<School> result = getFilterOutput(schools,query);
            Collections.sort(result);

            if (result.size() == 0){
                schoolError.setVisibility(View.VISIBLE);
                schoolError.setText("No data found, try search '*'");
                rvSchools.setVisibility(View.INVISIBLE);
            } else {
                schoolError.setVisibility(View.INVISIBLE);
                rvSchools.setVisibility(View.VISIBLE);
            }
            mLoadingProgress.setVisibility(View.INVISIBLE);
            SchoolAdapter adapter = new SchoolAdapter(result);
            rvSchools.setAdapter(adapter);


        } catch (Exception e){
            mLoadingProgress.setVisibility(View.INVISIBLE);
            Log.d("Error FVU", e.getMessage());
        }

        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }


    public class SchoolTask extends AsyncTask<URL, Void, String> {

        //this fx has no access to UI
        @Override
        protected String doInBackground(URL... urls){
            URL searchURL = urls[0];
            URL statURL = urls[1];

            String schoolStats = "", schoolList = "";
            Context context = getApplicationContext();

            //Next to call should be handle in parallel to avoid delay in loading the info
            //The reason i am calling both in here is because i want to put in diff color those schools who has no stats and avoid the click if the school is missing
            //in data

            //there should be an option to refresh the data on demand but as for now will be cache to smooth the experience
            try{
                schoolList = SpUtil.getPreferenceString(context, "schoolList");
                if (schoolList.isEmpty()) {
                    schoolList = ApiUtil.getJson(searchURL);
                    SpUtil.setPreferenceString(context, "schoolList",schoolList);
                }
            } catch (IOException e) {
                Log.d("Error", e.getMessage());
            }


            //see the comments from above
            //placing in 2 different "try", just to show the diff calls. should be done in the same block
            try{
                schoolStats = SpUtil.getPreferenceString(context, "schoolStats");
                if (schoolStats.isEmpty()) {

                    schoolStats = ApiUtil.getJson(statURL);
                    SpUtil.setPreferenceString(context, "schoolStats",schoolStats);
                }

            } catch (IOException e) {
                Log.d("Error", e.getMessage());
            }

            JSONObject resultJSON = new JSONObject();
            try{
                resultJSON.put("schoolList", schoolList);
                resultJSON.put("schoolStats", schoolStats);
            } catch (Exception e){

            }


            return resultJSON.toString();
        }

        //this will execute once doInBackground finish
        @Override
        protected void onPostExecute(String jsonResult){

            TextView schoolError = (TextView) findViewById(R.id.schoolError);

            if (jsonResult == null){
                schoolError.setVisibility(View.VISIBLE);
                rvSchools.setVisibility(View.INVISIBLE);
            } else {
                schoolError.setVisibility(View.INVISIBLE);
                rvSchools.setVisibility(View.VISIBLE);
            }

            mLoadingProgress.setVisibility(View.INVISIBLE);


            ArrayList<School> schools = ApiUtil.getSchoolListFromJson(jsonResult,  ApiUtil.getSchoolStatsFromJson(jsonResult));
            Collections.sort(schools);

            SchoolAdapter adapter = new SchoolAdapter(schools);
            rvSchools.setAdapter(adapter);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mLoadingProgress.setVisibility(View.VISIBLE);
        }
    }

}
